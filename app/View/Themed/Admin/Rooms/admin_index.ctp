<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">
                <div class="x_title">
                    <h2>Rooms</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addRoom">Add Room</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="roomtable" class="table table-striped table-bordered">
                        <thead>
                            <tr role="row">
                                <th>Id</th>
                                <th>Number</th>
                                <th>Type</th>
                                <th>Rent</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!---------------ADD MODAL ------------->
<div id="addRoom" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Room</h4>
            </div>
            <div class="modal-body col-sm-12">
                <fieldset class="col-sm-12">
                        <form method="post" action="" id="add_room">
                            <div class="form-group col-sm-12" >
                                <label class="control-label">Type</label>
                                <input id="room_type" name="room_type" type="text"  class="form-control" data-validation="char">
                                <span id="alert_room_type"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Room NO</label>
                                <input id="room_no" name="room_no" type="text" required="required" class="form-control" data-validation="postal">
                                <span id="alert_room_no"></span>
                            </div>
                            <div class="form-group col-sm-6" >
                                <label class="control-label">Rent</label>
                                <input id="room_rent" name="room_rent" type="text"  class="form-control" data-validation="float">
                                <span id="alert_room_rent"></span>
                            </div>
                        </form>
                    </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info btn-lg" id="add_room_btn">Add</button>
                <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!------------ END MODAL --->
<script type="text/javascript">
    $('#add_room_btn').attr('disabled',true);
    var dataTable = $('#roomtable').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true, //false
        "columnDefs": [{
            "targets": 0,
            "orderable": true, //false
            "searchable": true //false

        }],
        "ajax": {
            url: "<?= $this->Html->Url('/',true) ?>admin/rooms/room_ajaxtable", // json datasource
            type: "post", // method  , by default get
            error: function() { // error handling

            }
        }
    });
    $('#add_room_btn').on('click',function(){
        var data = $('#add_room').serializeArray();
        // console.log(data);
        $.ajax({
            type: "POST",
            url: "<?= $this->Html->url('/',true) ?>admin/rooms/add",
            data: $("#add_room").serializeArray(),
            success: function(data) {
                console.log(data);
                dataTable.ajax.reload();
                var result = JSON.parse(data);
                if (result.status == 1) {
                    swal("Success!", result.message, "success");
                    $('#addRoom').modal('toggle');
                } else {
                    swal("Warning!", result.message, "warning"); 
                    $('#addRoom').modal('toggle');
                }
            },
            error: function(data){
                // var result = JSON.parse(data);
                swal("Warning!", "Something went wrong", "warning");    
            }
        });
    });
    $('#room_no').on('keyup',function(){
       var room_no=$(this).val();
       $.ajax({
            type: "POST",
            url: "<?= $this->Html->url('/',true) ?>admin/rooms/valid_room",
            data: {room_no:room_no},
            success: function(data) {
                console.log(data);
                if(data==1){
                    $('#add_room_btn').attr('disabled',true);
                }else if(data==0){
                    $('#add_room_btn').attr('disabled',false);
                }
            }
        });
    });
    $(document).on('click','.delete',function(){
        var id=$(this).attr('data-val');
        $.ajax({
            type: "GET",
            url: "<?= $this->Html->url('/',true) ?>admin/rooms/delete_room/"+id+"/Delete",
            // data: {room_no:room_no},
            success: function(data) {
                console.log(data);
                if(data==1){
                   swal("deleted success");
                   dataTable.ajax.reload();
                }else if(data==0){
                    swal("Unable to delete");
            }
        }
    });
});
    // } );
</script>
