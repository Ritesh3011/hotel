<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">
                <div class="x_title">
                    <h2>Bookings</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <fieldset class="col-sm-6">
                        <form method="post" action="<?= $this->Html->url('/',true) ?>admin/Customer/add" id="add_customer">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Customer Name</label>
                                <input id="customer_name" name="customer_name" type="text" required="required" class="form-control">
                                <span id="alert_Customer_name"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Company Name</label>
                                <input id="company_name" name="company_name" type="text" required="required" class="form-control">
                                <span id="alert_company_name"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Address</label>
                                <input id="address" name="address" type="text" required="required" class="form-control">
                                <span id="alert_address"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Contact no.</label>
                                <input id="contact" name="contact" type="text" required="required" class="form-control">
                                <span id="alert_contact"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Email</label>
                                <input id="email" name="email" type="text" required="required" class="form-control">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">AlternateContact no.</label>
                                <input id="alter_contact" name="alter_contact" type="text" required="required" class="form-control">
                                <span id="alert_contact"></span>
                            </div>
                            <div class="form-group col-sm-6" id="customer_id_area">
                                <label class="control-label">Customer_id no.</label>
                                <input id="customer_id" name="customer_id" type="text" readonly="" class="form-control">
                                <span id="alert_contact"></span>
                            </div>
                            <div class="form-group col-sm-6">

                                <input type="button" name="submit" class="btn btn-success pull-right" value="Add Customer" id="add_customer_submit">
                            </div>
                        </form>
                    </fieldset>
                    <fieldset class="col-sm-6">
                        <form id="book_room" method="post">
                            <input type="hidden" name="customer_id" id="fetch_customer_id" value="0">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Checkin Date</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="">
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Checkout Date</label>
                                <input id="check_out_date" name="check_out_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="">
                                <span id="alert_check_out_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Room type</label>
                                <select class="form-control" name="room_type" id="select_room_type">
                                    <option value=""> Select</option>
                                    <?php foreach ($room_type as $room): ?>
                                        <option value="<?= $room['Room']['id'] ?>">
                                            <?= $room['Room']['type'] ?>
                                        </option>
                                        <?php endforeach ?>
                                </select>
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Room(qty)</label>
                                <input id="no_of_room" name="no_of_room" type="text" required="required" class="form-control" value="1">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Extra Person</label>
                                <input id="extra_person" name="extra_person" type="text" required="required" class="form-control">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Extra Bed</label>
                                <input id="extra_bed" name="extrabed" type="text" required="required" class="form-control">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Sharing</label>
                                <input id="sharing" name="sharing" type="text" required="required" class="form-control">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Meal Plan</label>
                                <!-- <inputtype="text" required="required" class="form-control"> -->
                                <select class="form-control" name="room_type" id="meal_type" name="meal_plan]">
                                    <?php foreach ($meal_type as $meal): ?>
                                        <option value="<?= $meal['MealPlan']['id'] ?>"><?= $meal['MealPlan']['name'] ?></option>
                                        <?php endforeach ?>
                                </select>
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Room Rent</label>
                                <input id="room_rent" name="room_rent" type="text" required="required" class="form-control" readonly="">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Rent Payable</label>
                                <input id="rent_payable" name="rent_payable" type="text" required="required" class="form-control">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Total amount</label>
                                <input id="rent_payable" name="total_amount" type="text" required="required" class="form-control">
                                <span id="alert_"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <input type="button" id="btn_room_book" class="btn btn-primary col-12" name="ADD" value="ADD" style="display: none;">
                            </div>
                        </form>
                    </fieldset>
                </div>
            </div>

        </div>
    </div>

    <!-------------FOR DATA TABLE ---------------->
    <div class="row" style="" id="booking_details">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Bookings List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="roomtable" class="table table-striped table-bordered">
                        <thead>
                            <tr role="row">
                                <th>Sl</th>
                                <th>Booking Id</th>
                                <th>Checkin</th>
                                <th>Checkout</th>
                                <th>Room Type</th>
                                <th>Room+</th>
                                <th>Person+</th>
                                <th>Bed+</th>
                                <th>Meal Plan</th>
                                <th>Rent(payable)</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <!-------------------------->
</div>

<script type="text/javascript">
var dataTable = 123;
    $('document').ready(function() {
        $('#check_in_date').datepicker();
        $('#check_out_date').datepicker();
         console.clear();
    });
    $('#add_customer_submit').on('click', function() {
        var customer = {};
        $.each($('#add_customer').serializeArray(), function(ccc) {
            customer[ccc.name] = ccc.value;
            if ($(ccc.name).val()) {
                console.log(ccc.value);
                // $('#'+customer[this.name]).attr("style","background-color:red");
            }
        });
        $.ajax({
            type: "POST",
            url: "<?= $this->Html->url('/',true) ?>admin/Customer/add",
            data: $("#add_customer").serializeArray(),
            success: function(data) {
                console.log(data);
                var result = JSON.parse(data);
                if (result.result == 'success') {
                    swal("Success!", "Customer Created: id=" + result.last_insert_id, "success", {
                        button: "Aww yiss!"
                    });
                    $('#fetch_customer_id').val(result.last_insert_id);
                    $('#customer_id').val(result.last_insert_id);
                    $('#btn_room_book').show();
                    $('#add_customer_submit').hide();
                    dataTable= $('#roomtable').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": true, //false
                "columnDefs": [{
                    "targets": 0,
                    "orderable": true, //false
                    "searchable": true //false
                }],
                "ajax": {
                    url: "<?= $this->Html->Url('/',true) ?>admin/customer/room_ajaxtable/"+result.last_insert_id, // json datasource
                    type: "post", // method  , by default get
                    error: function() { // error handling

                    }
                }
            });
                } else {

                }
            }
        });
    });
    $('#btn_room_book').on('click', function() {
        var id=$('#customer_id').val();
        // var form_data=$('#book_room').serializeArray();
        var result = {};
        $.each($('#book_room').serializeArray(), function() {
            result[this.name] = this.value;
        });
        console.log(result);
        $.ajax({
            type: "POST",
            url: "<?= $this->Html->url('/',true) ?>admin/Customer/room_book",
            data: $("#book_room").serializeArray(),
            success: function(data) {
                console.log(data);
                dataTable.ajax.reload();

                var result = JSON.parse(data);
                if (result.result == 'success') {
                    alert(1);
                    $('#customer_id').val(result.last_insert_id);
                } else {

                }
            }
        });
    });
    $('#select_room_type').on('change',function(){
        var id=$(this).val();
        $.ajax({
            type: "POST",
            url: "<?= $this->Html->url('/',true) ?>admin/rooms/room_details",
            data: {id:id},
            success: function(data) {
                var result = JSON.parse(data);
                if (result.Room.id > 0) {
                    console.log(data);
                    $('#room_rent').val(result.Room.rent);
                    $('#rent_payable').val(result.Room.rent);
                } else {

                }
            }
        });
    }); 
</script>