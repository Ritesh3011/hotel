<div class="right_col" role="main">
    <div class="row" style="" id="booking_details">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Booking List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="roomtable" class="table table-striped table-bordered">
                        <thead>
                            <tr role="row">
                                <th>Sl</th>
                                <th>Booking Id</th>
                                <th>Checkin</th>
                                <th>Checkout</th>
                                <th>Room Type</th>
                                <th>Room+</th>
                                <th>Person+</th>
                                <th>Bed+</th>
                                <th>Meal Plan</th>
                                <th>Rent(payable)</th>
                                <th>Ckeckin</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <!-------------------------->
</div>

<script type="text/javascript">

 // $(document).on('ready',function(){
     dataTable= $('#roomtable').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": true, //false
                "columnDefs": [{
                    "targets": 0,
                    "orderable": true, //false
                    "searchable": true //false

                }],
                "ajax": {
                    url: "<?= $this->Html->Url('/',true) ?>admin/customer/room_ajaxtable/0", // json datasource
                    type: "post", // method  , by default get
                    error: function() { // error handling

                    }
                }
            });

  $(document).on("click", ".check_in", function(){
            var id=$(this).attr('data-val');
            // windows.location()
            window.location.href = "<?= $this->Html->Url('/',true) ?>admin/customer/checkin_details/"+id;
         });
</script>