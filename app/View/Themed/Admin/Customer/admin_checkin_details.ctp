<div class="right_col" role="main">
    <!-------------FOR DATA TABLE ---------------->
    <div class="row" style="" id="booking_details">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Check in <b style="color:green;"><?= $booking_details['RoomBook']['booking_id'] ?></b></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <fieldset class="col-sm-12" style="border: 4px solid #ededed;padding: 8px;">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Checkin Date</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="" value="<?= $booking_details['RoomBook']['check_in_date'] ?>">
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Checkout Date</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="" value="<?= $booking_details['RoomBook']['check_out_date'] ?>">
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Name</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="" value="<?= $booking_details['Customer']['customer_name'] ?>">
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Phone No</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="" value="<?= $booking_details['Customer']['contact'] ?>">
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Email</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="" value="<?= $booking_details['Customer']['email'] ?>">
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Room Type</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" readonly="" value="<?= $booking_details['RoomBook']['room_type'] ?>">
                                <span id="alert_check_in_date"></span>
                            </div>

                    </fieldset>
                    <fieldset class="col-sm-6" style="border: 4px solid #ededed;padding: 8px;">
                            <div class="form-group col-sm-12">
                                <label class="control-label">Booking Source</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">Porpose of visit</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">No of Male</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">No of female</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">No of clild</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                    </fieldset>
                    <fieldset class="col-sm-6" style="border: 4px solid #ededed;padding: 8px;">
                            <div class="form-group col-sm-12">
                                <label class="control-label">Arrival From</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">Id Proof Type</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">Id proof NO</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">Going to</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label">Special Instruction</label>
                                <input id="check_in_date" name="check_in_date" type="text" required="required" class="form-control" data-date-format="yyyy-mm-dd" >
                                <span id="alert_check_in_date"></span>
                            </div>
                    </fieldset>
                    <fieldset class="col-sm-12">
                        <button type="button" class="btn btn-primary btn-lg">CHECK IN</button>
                    </fieldset>
                </div>
            </div>

        </div>
    </div>
    <!-------------------------->
</div>
<?php //pr($booking_details); ?>
<script type="text/javascript">

</script>