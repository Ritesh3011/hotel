        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?= $this->Html->url('/admin') ?>" class="site_title"><i class="fa fa-paw"></i> <span>Hotel OYO!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?= $this->Html->url('/',true) ?>images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-user"></i> Customer <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= $this->Html->Url('/admin/customer/book') ?>">Booking</a></li>
                      <li><a href="<?= $this->Html->url('/admin/customer/checkin') ?>">Checkin</a></li>
                      <li><a href="<?= $this->Html->url('/admin/customer/checkout') ?>">Checkout</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i>Account Management<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tables.html">Purchase Report</a></li>
                      <li><a href="tables_dynamic.html">Profit and loss</a></li>
                      <li><a href="tables_dynamic.html">Payments</a></li>
                      <li><a href="tables_dynamic.html">Bill</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-table"></i>Room<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= $this->Html->Url('/admin/rooms') ?>">Room</a></li>
                    </ul>
                  </li>

                </ul>
              </div>
<!--               <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_403.html">403 Error</a></li>
                      <li><a href="page_404.html">404 Error</a></li>
                      <li><a href="page_500.html">500 Error</a></li>
                      <li><a href="plain_page.html">Plain Page</a></li>
                      <li><a href="login.html">Login Page</a></li>
                      <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
              </div> -->

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>





    <script src="<?= $this->Html->url('/',true) ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- FastClick -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= $this->Html->url('/',true) ?>vendors/moment/min/moment.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?= $this->Html->url('/',true) ?>build2/js/custom.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/datepicker/datepicker.js"></script>
    <script src="<?= $this->Html->url('/',true) ?>vendors/sweetalert/sweetalert.min.js"></script>