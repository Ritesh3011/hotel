<!DOCTYPE html>
<html class="" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Bootstrap Admin App + jQuery">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <link rel="icon" href="<?= $this->Html->url('/',true) ?>favicon.ico" type="image/x-icon">
    <title>Hotel</title>
    <!--===============VENDOR STYLES===============-->
    <!-- FONT AWESOME-->
    <?php 
    echo $this->Html->script(array('pp/jquery-2.js','pp/jquery_004.js','pp/bootstrap.min.js'));
    echo $this->Html->css(array('pp/font-awesome.css',
     'pp/simple-line-icons.css',
      'pp/animate.css',
      'pp/whirl.css',
      'pp/index.css',
      'pp/weather-icons.css',
      // 'pp/dataTables1.css',
      // 'pp/dataTables_002.css',
      'pp/bootstrap.css',
      
      'pp/app.css',
      // 'pp/theme-d.css',
      'pp/bootstrap-datetimepicker.css',
      // 'bootstrap-combined.no-icons.min.css',
      'vendor/sweetalert/sweetalert.min.css',

  ));
    
    ?>
    
    <!-- <link rel="stylesheet" type="text/css" href="/pp/debug_toolbar.css"> -->
</head>

<body>
    <div class="wrapper">
        <!-- top navbar-->
        <header class="topnavbar-wrapper">
            <!-- START Top Navbar-->
            <nav role="navigation" class="navbar topnavbar">
                <!-- START navbar header-->
                <div class="navbar-header">
                    <a href="#" class="navbar-brand">
                        <div class="brand-logo"><h4 style="color:white;">HOTEL</h4></div>
                        <div class="brand-logo-collapsed"><h4 style="color:white;">H</h4><!-- <img src="/pp/logoicon.png" alt="App Logo" class="img-responsive"> --></div>
                    </a>
                </div>
                <!-- END navbar header-->
                <!-- START Nav wrapper-->
                <div class="nav-wrapper">
                    <!-- START Left navbar-->
                    <ul class="nav navbar-nav">
                        <li>
                            <!-- Button used to collapse the left sidebar.Only visible on tablet and desktops--><a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs mybtn2"><fa class="fa fa-navicon"></fa></a>
                            <!-- Button to show/hide the sidebar on mobile.Visible on mobile only.--><a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle"><em class="fa fa-navicon"></em></a></li>
                        <!-- START User avatar toggle-->
                        <li>
                            <!-- Button used to collapse the left sidebar.Only visible on tablet and desktops--><a id="user-block-toggle" href="#user-block" data-toggle="collapse" aria-expanded="false" class="collapsed"><em class="icon-user"></em></a></li>
                        <!-- END User avatar toggle-->
                    </ul>
                    <!-- END Left navbar-->
                    <!-- START Right Navbar-->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?= $this->Html->url('/admin/Users/logout') ?>" title="Lock screen">Logout<em class="icon-lock"></em></a></li>
                    </ul>
                    <!------------IDLE START----------->
                    <!-- <script type="text/javascript" src="/"></script> -->
                    <!---------IDLEEND--------->
                    <!-- END Right Navbar-->
                </div>
                <!-- END Nav wrapper-->
                <!-- START Search form-->
                <!-- END Search form-->
            </nav>
            <!-- END Top Navbar-->
        </header>
        <!-- sidebar-->
        <style type="text/css">
            .user-block.user-block-info.user-block-name {
                color: #737373;
                margin-bottom: 6px
            }
        </style>