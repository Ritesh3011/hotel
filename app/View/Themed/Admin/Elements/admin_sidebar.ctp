<aside class="aside">
            <!-- START Sidebar(left)-->
<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
            <!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
            <div class="aside-inner">
                <nav data-sidebar-anyclick-close="" class="sidebar">
                    <!-- START sidebar nav-->
                    <ul class="nav" id="menu">
                        <!-- START user info-->
                        <li class="has-user-block">
                            <div id="user-block" class="collapse" aria-expanded="false" style="height: 0px;">
                                <div class="item user-block">
                                    <!-- User picture-->
                                    <div class="user-block-picture">
                                        <div class="user-block-status"><img src="" alt="1498063435thumb.jpg" class="img-thumbnail img-circle" width="60" height="60">
                                            <div class="circle circle-success circle-lg"></div>
                                        </div>
                                    </div>
                                    <!-- Name and Job-->
                                    <div class="user-block-info"><span class="user-block-name ">Administrator</span><a href="#" class="btn btn-danger"><strong>Profile Edit</strong></a></div>
                                </div>
                            </div>
                        </li>
                        <!-- END user info-->
                        <!-- Iterates over all sidebar items-->
                        <!--<li class="nav-heading "><span data-localize="sidebar.heading.HEADER">Main Navigation</span></li>-->
                        <li class=""><a href="" data-toggle=""><em class="icon-user"></em><span data-localize="sidebar.nav.DASHBOARD">Dashboard</span></a></li>
                        <!----------------->
                        <li class="<?php echo ($this->params->controller=='customer'?"active":""); ?>"><a href="#customer_management" data-toggle="collapse" ><em class="icon-book"></em><span data-localize="sidebar.nav.element.ELEMENTS">Customer Management</span></a>
                            <ul id="customer_management" class="nav sidebar-subnav collapse" >
                                <li class="sidebar-subnav-header">Customer</li>
                                <li class="<?php echo ($this->params->action=='admin_book'?"active":""); ?>"><a href="<?= $this->Html->url('/admin/customer/book') ?>"><em class="icon-briefcase"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Booking</span></a></li>
                                <li class=""><a href="<?= $this->Html->url('/admin/customer/checkin') ?>"><em class="icon-briefcase"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Checkin</span></a></li>
                                <li class=""><a href=""><em class="icon-briefcase"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Checkout</span></a></li>
                                <li class=""><a href=""><em class="icon-briefcase"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Update room status</span></a></li>
                                <li class=""><a href=""><em class="icon-briefcase"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Edit Booking</span></a></li>
                           </ul>
                        </li>


                        <li class="<?php echo ($this->params->controller=='rooms'?"active":""); ?>">
                           <a href="#sidebar_account" data-toggle="collapse">
                              <em class="fa fa-inr"></em>
                              <span data-localize="sidebar.nav.element.ELEMENTS">Account Management</span>
                           </a>
                           <ul id="sidebar_account" class="nav sidebar-subnav collapse">
                              <!-- <li class="sidebar-subnav-header">Subscribers</li> -->
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="fa fa-money"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Purchase Report</span>
                                 </a>
                              </li>   
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="fa fa-google-wallet"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Profit and loss</span>
                                 </a>
                              </li>
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="fa fa-credit-card"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Payment</span>
                                 </a>
                              </li> 
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Bills</span>
                                 </a>
                              </li> 
                           </ul>
                        </li>



                        <li class="<?php //echo ($this->params->controller=='rooms'?"active":""); ?>">
                           <a href="#sidebar_raw_materials" data-toggle="collapse">
                              <em class="icon-people"></em>
                              <span data-localize="sidebar.nav.element.ELEMENTS">Raw Materials</span>
                           </a>
                           <ul id="sidebar_raw_materials" class="nav sidebar-subnav collapse">
                              <!-- <li class="sidebar-subnav-header">Subscribers</li> -->
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Purchase Entry</a>
                              </li> 
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Deliver to Kitchen</a>
                              </li>
                               <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Return Back</a>
                              </li> 
                               <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Materials Add</a>
                              </li>  
                               <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Damage Entry</a>
                              </li> 
                               <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Bar stock entry</a>
                              </li>
                               <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION"></span>Bar product entry</a>
                              </li>  
                           </ul>
                        </li>

                        <li class="<?php echo ($this->params->controller=='rooms'?"active":""); ?>">
                           <a href="#sidebar_room" data-toggle="collapse">
                              <em class="icon-people"></em>
                              <span data-localize="sidebar.nav.element.ELEMENTS">Rooms</span>
                           </a>
                           <ul id="sidebar_room" class="nav sidebar-subnav collapse">
                              <!-- <li class="sidebar-subnav-header">Subscribers</li> -->
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="<?= $this->Html->url('/admin/rooms') ?>">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Rooms</span>
                                 </a>
                              </li>                       
                           </ul>
                        </li>

                        <li class="<?php echo ($this->params->controller=='rooms'?"active":""); ?>">
                           <a href="#sidebar_food_menu" data-toggle="collapse">
                              <em class="icon-people"></em>
                              <span data-localize="sidebar.nav.element.ELEMENTS">Food Menu</span>
                           </a>
                           <ul id="sidebar_food_menu" class="nav sidebar-subnav collapse">
                              <!-- <li class="sidebar-subnav-header">Subscribers</li> -->
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Add new Menu</span>
                                 </a>
                              </li>   
                              <li class="<?php //echo ($this->params->action=='admin_index'?"active":""); ?>">
                                 <a href="#">
                                    <em class="icon-user-following"></em><span data-localize="sidebar.nav.element.NOTIFICATION">Update a menu</span>
                                 </a>
                              </li>                       
                           </ul>
                        </li>

                        <!------------------>
                    </ul>
                    <!-- END sidebar nav-->
                </nav>
            </div>
            <!-- END Sidebar(left)-->
        </aside>


        