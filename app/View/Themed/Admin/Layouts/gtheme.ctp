 <?php echo $this->element('g_admin_header'); ?>
   <body class="nav-md">
    <div class="container body">
      <div class="main_container">

			<?php echo $this->element('g_admin_sidebar'); ?>
			 <!-- top navigation -->
				<?php echo $this->element('g_admin_navbar'); ?>
			<!-- /top navigation -->
	<?php echo $this->fetch('content'); ?>
	
<?php echo $this->element('g_admin_footer'); ?>