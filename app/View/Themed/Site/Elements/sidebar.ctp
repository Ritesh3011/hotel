    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"><span>
                            <img alt="image" class="img-circle" src="img/profile.png" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin</strong>
                             </span> <!-- <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> --> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="logout.php">Logout</a></li>
                            </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class="active">
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Customer Management</span></a>
                    <ul>
                        <li><a href="#">Booking</a></li>
                        <li><a href="#">Checkin</a></li>
                        <li><a href="#">Checkout</a></li>
                        <li><a href="#">Update room status</a></li>
                        <li><a href="#">Edit Booking</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">Account Management</span> </a>
                    <ul>
                        <li><a href="#">Payment</a></li>
                        <li><a href="#">Bill</a></li>
                    </ul>
                </li>
                <li>
                    <a href="product_category.php"><i class="fa fa-arrows"></i> <span class="nav-label">Product Category</span> </a>
                </li>
                <li>
                    <a href="products.php"><i class="fa fa-diamond"></i> <span class="nav-label">Products</span> </a>
                </li>
                <li>
                    <a href="latest_collections.php"><i class="fa fa-bolt"></i> <span class="nav-label">Latest Collections</span> </a>
                </li>
                <li>
                    <a href="newsletter.php"><i class="fa fa-rocket"></i> <span class="nav-label">Newsletter Subscriber</span> </a>
                </li>
            </ul>
        </div>
    </nav>