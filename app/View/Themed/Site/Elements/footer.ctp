<div class="footer">
            <div class="pull-right">
                <!-- 10GB of <strong>250GB</strong> Free. -->
            </div>
            <div>
                <strong>Copyright</strong> Rishi's &copy; 2018
            </div>
        </div>

    </div>

</div>
</body>
<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<!-- Toastr -->
<script src="js/plugins/toastr/toastr.min.js"></script>
<!-- blueimp gallery -->
<script src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<!-- Sweet alert -->
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- SUMMERNOTE -->
<script src="js/plugins/summernote/summernote.min.js"></script>
</body>
</html>
<!-- <script>
    $(document).ready(function () {
        setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success("Rishi's", 'Welcome to Dashboard');

            }, 1300);
    })

</script> -->