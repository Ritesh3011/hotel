<?php
/**
 * RoomBook Fixture
 */
class RoomBookFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'room_book';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'customer_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'no_of_room' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'extra_person' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'extrabed' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'sharing' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'meal_plan' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'room_type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'room_rent' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '6,2', 'unsigned' => false),
		'rent_payable' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '6,2', 'unsigned' => false),
		'is_cancelled' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'is_checked' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'booking_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'cancelation_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'customer_id' => 1,
			'no_of_room' => 1,
			'extra_person' => 1,
			'extrabed' => 1,
			'sharing' => 1,
			'meal_plan' => 1,
			'room_type' => 'Lorem ipsum dolor sit amet',
			'room_rent' => 1,
			'rent_payable' => 1,
			'is_cancelled' => 1,
			'is_checked' => 1,
			'booking_date' => '2019-01-22 02:28:55',
			'cancelation_date' => '2019-01-22 02:28:55'
		),
	);

}
