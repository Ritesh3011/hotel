<?php
App::uses('RoomBook', 'Model');

/**
 * RoomBook Test Case
 */
class RoomBookTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.room_book',
		'app.customer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RoomBook = ClassRegistry::init('RoomBook');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RoomBook);

		parent::tearDown();
	}

}
