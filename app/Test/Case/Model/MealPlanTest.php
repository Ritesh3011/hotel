<?php
App::uses('MealPlan', 'Model');

/**
 * MealPlan Test Case
 */
class MealPlanTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.meal_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MealPlan = ClassRegistry::init('MealPlan');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MealPlan);

		parent::tearDown();
	}

}
