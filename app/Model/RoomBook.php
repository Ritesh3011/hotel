<?php
App::uses('AppModel', 'Model');

class RoomBook extends AppModel {
	public $useTable = 'room_book';
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
