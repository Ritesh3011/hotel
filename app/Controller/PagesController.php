<?php
App::uses('AppController', 'Controller');
class PagesController extends AppController {
	public $components = array('DebugKit.Toolbar');
	public $uses = array('Users','Room','Customer','RoomBook');
	public function beforeRender() {
    parent::beforeRender();
}
	public function index()
	{
		$this->redirect('../admin');
	}
	public function admin_index()
	{
		$this->layout='gtheme';
		$customers=$this->Customer->find('all');
		$rooms=$this->Room->find('all');
		$users=$this->Users->find('all');
		$room_book=$this->RoomBook->find('all');
		$counter=array(
			'customer'=>sizeof($customers),
			'room'=>sizeof($rooms),
			'staff'=>sizeof($users),
			'booked'=>sizeof($room_book),
			'checked_in'=>sizeof($room_book),
		);
		$this->set(compact('counter'));
	}
}
