<?php


App::uses('Controller', 'Controller');

class AppController extends Controller {

	public $components = array(
        'Flash',
        'DebugKit.Toolbar',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'posts',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'pages',
                'action' => 'index',
                'home'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );

	public function beforefilter()
	{
		$this->Auth->allow('index', 'view'); ////Ritesh
		if(!empty($this->request->prefix))
	{
		 $this->theme = $this->request->prefix;//'admin';
	}
	else
	{
			 $this->theme = 'site';	
	}
	}
	public function index(){
		echo 1;
	}
}
