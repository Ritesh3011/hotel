<?php


App::uses('AppController', 'Controller');

class CustomerController extends AppController {
	public $uses = array('MealPlan','Room','Customer','RoomBook');

public function beforeRender() {
    parent::beforeRender();

  	  $this->layout = 'gtheme';
}
	public function index()
	{
		exit;
	} 
	public function admin_book()
	{
		$meal_type=$this->MealPlan->find('all');
		$room_type=$this->Room->find('all',array(
			// 'fields'=>array('DISTINCT type'),
		));

		$this->set(compact('meal_type','room_type'));
	} 
	public function admin_add()
	{
		$status=array();
		if($result=$this->Customer->save($this->request->data)){
			$status['result']='success';
			$status['last_insert_id']=$result['Customer']['id'];
			echo json_encode($status);
		}else{
			echo 0;
		}
// 		if ($this->Customer->save($this->request->data())) {
//     return $this->ModelName->id;
// }
// else {
//     debug($this->Customer->invalidFields());
//     return false;
// }
		exit;
	}
	public function admin_room_book()
	{
		// pr($_POST);
		$room_type=$this->Room->findById($this->request->data['room_type']);
		$room_rent=$room_type['Room']['rent']*$this->request->data['no_of_room'];
		// pr($room_type);
		$date1=date_create($this->request->data['check_in_date']);
		$date2=date_create($this->request->data['check_out_date']);
		$diff=date_diff($date1,$date2);
		if($diff->invert==0)
		{
			if($diff->days==0)
			{
				$total_days=1;
			}else{
				$total_days=$diff->days;
			}
		}else{
			echo 0;
			exit;
		}
		$data=array(
			'booking_id'		=>strtoupper(uniqid()),
			'customer_id'		=>$this->request->data['customer_id'],
			'check_in_date'		=>$this->request->data['check_in_date'],
			'check_out_date'	=>$this->request->data['check_out_date'],
			'room_type'			=>$this->request->data['room_type'],
			'no_of_room'		=>$this->request->data['no_of_room'],
			'extra_person'		=>$this->request->data['extra_person'],
			'extrabed'			=>$this->request->data['extrabed'],
			'sharing'			=>$this->request->data['sharing'],
			'room_rent'			=>$room_rent*$this->request->data['no_of_room']*$total_days,
			'meal_plan'			=>1,//$this->request->data['meal_plan'],
			'rent_payable'		=>$this->request->data['rent_payable'],
			'total_amount'		=>0,
		);
		if($this->RoomBook->save($data))
		{
			echo 1;
		}else{
			echo 0;
		}
			
					exit;
	}
	public function admin_room_ajaxtable($id)
	{
			// echo 'here';	
			if($id==0)
			{
				$rooms=$this->RoomBook->find("all",array(
					'conditions'=>array(
						// 'customer_id'=>$id
					),
					'limit'=>$this->request->data['length'],
				'offset'=>$this->request->data['start'],
				));
				$rooms_count=$this->RoomBook->find('count',array(
					'conditions'=>array(
						// 'customer_id'=>$id
					),
				));	
			}else{
				$rooms=$this->RoomBook->find("all",array(
					'conditions'=>array(
						'customer_id'=>$id
					),
				));
		
			$rooms_count=$this->RoomBook->find('count',array(
				'conditions'=>array(
					'customer_id'=>$id
				),
						));	
		}

						$data = array();
				$i=1;
				$totalRow = $rooms_count;
				foreach($rooms as $value)
				{
					$nestedData=array();
					$nestedData[] = ($this->request->data['start'])+$i++; 
					$nestedData[] = $value['RoomBook']['booking_id'];
					$nestedData[] = $value['RoomBook']['check_in_date'];
					$nestedData[] = $value['RoomBook']['check_out_date'];
					$nestedData[] = $value['RoomBook']['room_type'];
					$nestedData[] = $value['RoomBook']['no_of_room'];
					$nestedData[] = $value['RoomBook']['extra_person'];
					$nestedData[] = $value['RoomBook']['extrabed'];
					$nestedData[] = $value['RoomBook']['meal_plan'];
					$nestedData[] = $value['RoomBook']['rent_payable'];
					$nestedData[] = '<i class="fa fa-eye check_in" title="checkin" style="color:green;" data-val="'.$value['RoomBook']['id'].'"></i>';
					
					$data[] = $nestedData;
				}	
				$json_data = array(
					"draw"            => intval($this->request->data['draw']),   
					"recordsTotal"    => intval( $totalRow ),  // total number of records
					"recordsFiltered" => intval( $totalRow ), 
					"data"            => $data   // total data array
				);
				echo json_encode($json_data);

		exit;
	}
	public function admin_checkin(){
		
	}

	public function admin_checkin_details($id){

		$booking_details=$this->RoomBook->findById($id);
		if($booking_details)
		{
			$this->set(compact('booking_details'));
		}
		
	}
}