<?php


App::uses('AppController', 'Controller');


class RoomsController extends AppController {
	

	public $uses = array();


public function beforeRender() {
    parent::beforeRender();
    $this->Auth->user();
  	  $this->layout = 'gtheme';
}
	public function admin_index()
	{
		$rooms=$this->Room->find("all");
		$this->set(compact('rooms'));
	} 
	public function admin_add(){
		$user=$this->Auth->User();
		$response=array(
					'status'	=>1,
					'message'	=>'Unable to add',
				);
		if($user && $this->request->is('post')){
			$data=array(
				'no'	=>$this->request->data['room_no'],
				'type'	=>$this->request->data['room_type'],
				'rent'	=>$this->request->data['room_rent'],
			);
			$this->Room->create();
			if($this->Room->save($data)){
				$response=array(
					'status'	=>1,
					'message'	=>'Successfully Added',
				);
			}
		}
		echo json_encode($response);
		exit;
	}
	public function admin_room_ajaxtable()
	{
		if(!empty($this->request->data['search']['value']))
		{
			$search=$this->request->data['search']['value'];
			$rooms=$this->Room->find("all",array(
				'conditions'=>array(
					'OR'=>array(
						'no'=>$search,
						'type like'=>'%'.$search.'%',
						'rent'=>$search,
						),
					'AND'=>array(
					'is_deleted'=>0,
					// 'no'=>$room_no,
						),
				),
				'limit'=>$this->request->data['length'],
				'offset'=>$this->request->data['start'],
			));
			$rooms_count=$this->Room->find("count",array(
				'conditions'=>array(
					'OR'=>array(
						'no'=>$search,
						'type like'=>'%'.$search.'%',
						'rent'=>$search,
						),),
				// 'limit'=>$this->request->data['start'],
				// 'offset'=>$this->request->data['start'],
			));
		}else{
			// echo 'here';	
			$rooms=$this->Room->find("all",array(
				'conditions'=>array(
					'is_deleted'=>0,
				),
				'limit'=>$this->request->data['length'],
				'offset'=>$this->request->data['start'],
			));
			$rooms_count=$this->Room->find('count',array(
						));		
			}
						$data = array();
				$i=1;
				$totalRow = $rooms_count;
				foreach($rooms as $value)
				{
					$nestedData=array();
					$nestedData[] = ($this->request->data['start'])+$i++;
					$id=$value['Room']['id']; 
					$nestedData[] = $value['Room']['no'];
					$nestedData[] = $value['Room']['type'];
					$nestedData[] = $value['Room']['rent'];
					$nestedData[] = $value['Room']['status'];
					$nestedData[] = "<a href='javascript:void(0);'>
										<i class='fa fa-trash delete' data-val='$id' style='color:red;' ></i></a>";
					
					$data[] = $nestedData;
				}	
				$json_data = array(
					"draw"            => intval($this->request->data['draw']),   
					"recordsTotal"    => intval( $totalRow ),  // total number of records
					"recordsFiltered" => intval( $totalRow ), 
					"data"            => $data   // total data array
				);
				echo json_encode($json_data);

		exit;
	}
	public function admin_book()
	{

	} 
	public function admin_room_details(){
		$response=array('status'=>0);
		$id=$this->request->data['id'];
		if($this->request->is('post'))
		{
			$response=$this->Room->findById($id);
		}
		echo json_encode($response);
		die;
	}
	public function admin_valid_room(){
		if($this->request->is('post')){

			$room_no=$this->request->data['room_no'];
			if($room_no=='' || !is_numeric ($room_no))
			{
				echo 1;
				die;
			}
			$room=$this->Room->find('first',array(
				'conditions'=>array(
					'is_deleted'=>0,
					'no'=>$room_no,
				),
			));
			if($room){
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
		exit;
	}
	public function admin_delete_room($id,$status){
		if($this->request->is('get','POST','delete')){
			$room=$this->Room->findById($id);
			// pr($room);
			if($room){
				$room['Room']['is_deleted']=1;
				if($this->Room->save($room)){
					echo 1;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
		exit;
	}

}